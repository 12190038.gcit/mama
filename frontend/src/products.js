const products = [
  {
    _id: "1",
    name: "Vitamin A Capsule",
    image: "/images/v1.png",
    description:
      "The Recommended Dietary Allowance (RDA) for vitamin A is 900 mcg and 700 mcg per day for men and women, respectively. This intake level is easy to reach if you consume plenty of whole foods ( 28 ). ",
    brand: "Apple",
    category: "Vitamins",
    price: 89.99,
    countInStock: 10,
    rating: 4.5,
    numReviews: 12,
  },
  {
    _id: "2",
    name: "Paracetomol",
    image: "/images/t7.png",
    description:
      " Paracetamol is a commonly used medicine that can help treat pain and reduce a high temperature (fever). It's typically used to relieve mild or moderate pain, such as headaches, toothache or sprains, and reduce fevers caused by illnesses",
    brand: "Apple",
    category: "Tablets",
    price: 199.5,
    countInStock: 7,
    rating: 4.0,
    numReviews: 8,
  },
  {
    _id: "3",
    name: "Pond Lotion",
    image: "/images/l1.jpg",
    description:
      "Ponds Light Moisturiser is a light sensorial cream from Ponds for soft, moisturised skin with non-oily fresh glow. It is enriched with Vitamin E which",
    brand: "Cannon",
    category: "Lotions",
    price: 929.99,
    countInStock: 5,
    rating: 3,
    numReviews: 12,
  },
  {
    _id: "4",
    name: "Detol Lotion",
    image: "/images/l3.jpg",
    description:
      "ANTISEPTIC LIQUID: Protects from 100 illness causing germs · DISINFECTANT LIQUID: Antiseptic Liquid used to disinfect household items and surfaces",
    brand: "Sony",
    category: "Lotions",
    price: 399.99,
    countInStock: 11,
    rating: 5,
    numReviews: 12,
  },
  {
    _id: "5",
    name: "Stomach Painkiller",
    image: "/images/t2.jpg",
    description:
      "For cramping from diarrhea, medicines that have loperamide (Imodium) or bismuth subsalicylate (Kaopectate or Pepto-Bismol) might make you feel better. For other types of pain, acetaminophen (Aspirin Free Anacin, Liquiprin, Panadol, Tylenol) might be helpful",
    brand: "Logitech",
    category: "Vitamins",
    price: 49.99,
    countInStock: 7,
    rating: 3.5,
    numReviews: 10,
  },
  {
    _id: "6",
    name: "N10-Capsule",
    image: "/images/t3.jpg",
    description:
      "Meet Echo Dot - Our most popular smart speaker with a fabric design. It is our most compact smart speaker that fits perfectly into small space",
    brand: "Amazon",
    category: "Tablets",
    price: 29.99,
    countInStock: 0,
    rating: 4,
    numReviews: 12,
  },
];

export default products;
